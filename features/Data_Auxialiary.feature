@intg
Feature:Verify  Auxiliary gateway data api end point

	@get_countries
	Scenario: Verify get countries  end point.		
	   #Given I set Authorization header with MID as 999999999997 and MKEY as K3QD6YWYHFD
	   Given I set Authorization header to 'Authorization'
	   And I set content-type header to application/json	  
	   When I get data for transaction /Auxiliary/Countries	 			
       Then response code should be 200
       And response header Content-Type should be application/json
       And response body path $.isoFullName should be AALAND ISLANDS
	   And response body path $.isoNumericCode should be 248
	   And response body path $.isoAlpha3Code should be ALA
	   

   @get_state_provinces
	Scenario: Verify get state/provinces  end point.		
	   Given I set Authorization header to 'Authorization'
	   And I set content-type header to application/json
       And I set countryCode header to 01	   
	   When I get data for transaction /Data/Auxiliary/Subdivisions?countryCode=01	 			
       Then response code should be 200
       And response header Content-Type should be application/json
       And response body path $.isoFullName should be []

   @get_ach_originators
	Scenario: Verify get ach originators  ensssd point.	
	   
	   Given I set Authorization header to 'Authorization'
	   And I set content-type header to application/json
       And I set countryCode header to 01	   
	   When I get data for transaction /Data/Auxiliary/Originators	 			
       And response header Content-Type should be application/json
       And response body path $.id should be 1084361710  
	   And response body path $.description should be Washington
	   And response body path $.type should be PPD
