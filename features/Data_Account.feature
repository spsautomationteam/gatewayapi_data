@intg
Feature:Verify  gateway data apis

	@get_account_summary
	Scenario: Verify get account summary end point.		
	   #Given I set Authorization header with MID as 999999999997 and MKEY as K3QD6YWYHFD
	   Given I set Authorization header to 'Authorization'
	   And I set content-type header to application/json	  
	   When I get data for transaction /Data/Account	 			
       Then response code should be 200
       And response header Content-Type should be application/json
       And response body path $.address should be A16500 San Pedro Suite 400
	   And response body path $.autoCloseHour should be 0
	   And response body path $.ach.isManualSettlementAllowed should be true
	   
	@update_accountdetails
	Scenario: Verify update account details end point.		
	    Given I set Authorization header to 'Authorization'
	   And I set content-type header to application/json
       And I set body to {   "xmlWebServiceEnabled": true,   "customerServiceNumber": "0011111111111" }    
	   When I put data for transaction /Data/Account	 			
       Then response code should be 200
	 
	@get_account_summary_with_gatewayid
	Scenario: Verify get accountsummarydetails with gatewayid end point.		
	   Given I set Authorization header to 'Authorization'
	   And I set content-type header to application/json
	   And I set gatewayId header to 999999999997	            
	   When I get data for transaction /Data/Account/999999999997
       Then response code should be 200
       And response header Content-Type should be application/json
       And response body path $.address should be A16500 San Pedro Suite 400
	   And response body path $.autoCloseHour should be 0
	   And response body path $.ach.isManualSettlementAllowed should be true 	
	   
	@get_payment_center_parts
	Scenario: Verify get accountsummarydetails with gatewayid end point.		
	   Given I set Authorization header to 'Authorization'
	   And I set content-type header to application/json
	   And I set gatewayId header to 999999999997	            
	   When I get data for transaction /Data/Account/PaymentCenterParts
       Then response code should be 200
       And response header Content-Type should be application/json 	   
	