@intg
Feature:Verify  any transaction like Auth,Sale, Credit or Force and do a void action to cancel it.
    
	@post-transaction-sale-credit-force-auth
	Scenario Outline: Verify Transaction  'Force', 'Sale','Credit' and 'Authorization' and  do a void action to cancel it.		
	   Given I set Authorization header with MID as 999999999997 and MKEY as K3QD6YWYHFD
	   And I set content-type header to application/json
	   And I set body to {"transactionId": "5656", "retail": {   "amounts": {      "total": 85.17    },   "authorizationCode": "565656",     "orderNumber": "5656",    "cardData": {   "number": "<cardNumber>",      "expiration": "<ExpDate>"  ,"cvv":"<cvvCode>"  },},     "transactionCode": "<txnCode>"}
	   When I post data for transaction /BankCard/Transactions	 			
       Then response code should be 201
       And response header Content-Type should be application/json
       And response body path $.status should be Approved       	   	        
	   And I set body to {  "transactionCode": "Void"}  
	   When I patch data /BankCard/Transactions
       Then response code should be 200
    Examples:
	|txnCode|cardNumber|cvvCode|ExpDate|
	|Sale|4111111111111111|123|0620|
	|Force|5499740000000057|123|0620|
	|Credit|6011000993026909|123|0620|
	|Authorization|371449635392376|1234|0620|
	
	
	    
	@post-transaction-sale-void-again-void
	Scenario Outline: Verify Transaction  'Force', 'Sale','Credit' and 'Authorization' and  do a void action to cancel it.		
	   Given I set Authorization header with MID as 999999999997 and MKEY as K3QD6YWYHFD
	   And I set content-type header to application/json
	   And I set body to {"transactionId": "5656", "retail": {   "amounts": {      "total": 66    },   "authorizationCode": "565656",     "orderNumber": "5656",    "cardData": {   "number": "4111111111111111",      "expiration": "0618"    },},     "transactionCode": "<txnCode>"}
	   When I post data for transaction /BankCard/Transactions	 			
       Then response code should be 201
       And response header Content-Type should be application/json
       And response body path $.status should be Approved       	   	        
	   And I set body to {  "transactionCode": "Void"}  
	   When I patch data /BankCard/Transactions
       Then response code should be 200
	   And I set body to {  "transactionCode": "Void"}  
	   When I patch data /BankCard/Transactions
       Then response code should be 404
    Examples:
	|txnCode|
	|Sale|
	
	@post-transaction-force-invalid-authorizationcode
	Scenario Outline: Verify Transaction  'Force' with invalid 
	   Given I set Authorization header with MID as 999999999997 and MKEY as K3QD6YWYHFD
	   And I set content-type header to application/json
	   #And I set body to {"transactionId": "5656", "retail": {   "amounts": {      "total": 66    },   "authorizationCode": "12345",     "orderNumber": "5656",    "cardData": {   "number": "4111111111111111",      "expiration": "0618"    },},     "transactionCode": "<txnCode>"}
	   And I set body to {"transactionId": "5656", "retail": {   "amounts": {      "total": 67    },   ,     "orderNumber": "5656",    "cardData": {   "number": "4111111111111111",      "expiration": "0618"    },},     "transactionCode": "<txnCode>"}
	   When I post data for transaction /BankCard/Transactions	 			
       Then response code should be 400
       And response header Content-Type should be application/json
       And response body path $.message should be No request content was found    	   	        
	Examples:
	|txnCode|	
	|Force|
	       
	@post-transaction-sale-void-invalid-json
	Scenario Outline: Verify Transaction  'Force', 'Sale','Credit' and 'Authorization' and  do a void with invalid json
	   Given I set Authorization header with MID as 999999999997 and MKEY as K3QD6YWYHFD
	   And I set content-type header to application/json
	   And I set body to {"transactionId": "5656", "retail": {   "amounts": {      "total": 102    },   "authorizationCode": "565656",     "orderNumber": "5656",    "cardData": {   "number": "4111111111111111",      "expiration": "0618"    },},     "transactionCode": "<txnCode>"}
	   When I post data for transaction /BankCard/Transactions	 			
       Then response code should be 201
       And response header Content-Type should be application/json
       Then response body path $.status should be Approved       	   	        
	   Then I set body to {  "transactionCode": "xyz"}  
	   When I patch data /BankCard/Transactions
       Then response code should be 400
	   Then response body path $.errorCode should be InvalidRequestData 
	   Then response body path $.errorDescription should be request: The TransactionCode field is required.; Error converting value 
    Examples:
	|txnCode|
	|Sale|

        
	@post-transaction-sale-ecommerce-tag-json
	Scenario Outline: Verify Transaction  'Force', 'Sale','Credit' and 'Authorization' and  with 'ecommerce' tag  do a void action to cancel it.		
	   Given I set Authorization header with MID as 999999999997 and MKEY as K3QD6YWYHFD
	   And I set content-type header to application/json
	   And I set body to {"transactionId": "5656", "eCommerce": {   "amounts": {      "total": 242    },   "authorizationCode": "565656",     "orderNumber": "5656",    "cardData": {   "number": "4111111111111111",      "expiration": "0618"    },},     "transactionCode": "<txnCode>"}
	   When I post data for transaction /BankCard/Transactions	 			
       Then response code should be 201
       And response header Content-Type should be application/json
       And response body path $.status should be Approved       	   	        
	   And I set body to {  "transactionCode": "Void"}  
	   When I patch data /BankCard/Transactions
       Then response code should be 200
    Examples:
	|txnCode|
	|Sale|
	|Force|
	|Credit|
	|Authorization|   
	
	@post-transaction-sale-healthcare-tag-json
	Scenario Outline: Verify Transaction  'Force', 'Sale','Credit' and 'Authorization' and  with 'ecommerce' tag  do a void action to cancel it.		
	   Given I set Authorization header with MID as 999999999997 and MKEY as K3QD6YWYHFD
	   And I set content-type header to application/json
	   And I set body to {"transactionId": "5656", "healthCare": {   "amounts": {      "total": 240    },   "authorizationCode": "565656",     "orderNumber": "5656",    "cardData": {   "number": "4111111111111111",      "expiration": "0618"    },},     "transactionCode": "<txnCode>"}
	   When I post data for transaction /BankCard/Transactions	 			
       Then response code should be 201
       And response header Content-Type should be application/json
       And response body path $.status should be Approved       	   	        
	   And I set body to {  "transactionCode": "Void"}  
	   When I patch data /BankCard/Transactions
       Then response code should be 200
    Examples:
	|txnCode|
	|Sale|
	|Force|
	|Credit|
	|Authorization| 
	

